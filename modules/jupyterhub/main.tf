resource "helm_release" "jupyterhub" {
  name              = "jupyterhub"
  chart             = "jupyterhub"
  repository        = "https://hub.jupyter.org/helm-chart/"
  create_namespace  = "true"
  namespace         = "${var.namespace_prefix}spark-jobs"
  dependency_update = "true"
  version           = "3.3.8"

  set {
    name = "ingress.enabled"
    value = "true"
  }

  set {
    name = "ingress.ingressClassName"
    value = "nginx"
  }

  set {
    name = "ingress.hosts[0]"
    value = "jupyter.${var.domain}"
  }

  set {
    name = "singleuser.image.name"
    value = "jupyter/all-spark-notebook"
  }

  set {
    name = "singleuser.image.tag"
    value = "spark-3.5.0"
  }

  set {
    name = "singleuser.cmd"
    value = "start-notebook.py"
  }

  set {
    name = "singleuser.serviceAccountName"
    value = "spark"
  }
}