resource "helm_release" "minio_operator" {
  name              = "minio-operator"
  chart             = "operator"
  repository        = "https://operator.min.io/"
  create_namespace  = "true"
  namespace         = "minio-operator"
  dependency_update = "true"
  version           = "6.0.4"
}