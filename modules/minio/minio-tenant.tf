resource "helm_release" "minio_tenant" {
  depends_on = [ helm_release.minio_operator ]
  name              = "minio-tenant"
  chart             = "tenant"
  repository        = "https://operator.min.io/"
  create_namespace  = "true"
  namespace         = "${var.namespace_prefix}minio-tenant"
  dependency_update = "true"
  version           = "6.0.4"

  values = [
    file("./envs/${var.env}/minio-tenant.yaml")
  ]

  set {
    name = "ingress.api.host"
    value = "minio.${var.domain}"
  }

  set {
    name = "ingress.console.host"
    value = "console-minio.${var.domain}"
  }

  dynamic "set" {
    for_each = var.buckets
    content {
      name  = "tenant.buckets[${set.key}].name"
      value = set.value.name
    }
  }


  # set {
  #   name = "tenant.buckets[0].name"
  #   value =  "workflow-logs"
  # }

  # set {
  #   name = "tenant.buckets[1].name"
  #   value =  "binaries"
  # }


  # ,"binaries","graph","pippo"]
}