variable "env" {
  type    = string
  default = "local"
}

variable "kube_config" {
  type    = string
  default = "~/.kube/config"
}

variable "kube_context" {
  type    = string
  default = "default"
}

variable "namespace_prefix" {
  type    = string
  default = "lot1-"
}

variable "domain" {
  type    = string
  default = "local-dataplatform"
}

variable "s3_endpoint" {
  type    = string
  default = "https://minio.dnet-minio-tenant.svc.cluster.local"
}

variable "s3_key" {
  type    = string
  default = "minio"
}

variable "s3_secret" {
  type    = string
  default = "minio123"
}

variable "admin_user" {
  type    = string
}

variable "admin_password" {
  type    = string
}

variable "admin_hash" {
  type    = string
}

variable "repo_url" {
  type    = string
  default = "https://code-repo.d4science.org/D-Net/code-infrastructure-lab.git"
}

variable "branch_name" {
  type    = string
  default = "master"
}

variable "dag_path" {
  type    = string
  default = "workflow/dnet"
}
