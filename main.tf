module "minio" {
  source           = "./modules/minio"
  kube_context     = var.kube_context
  namespace_prefix = var.namespace_prefix
  buckets          = var.minio_buckets
}


module "airflow" {
  source           = "./modules/airflow"
  kube_context     = var.kube_context
  admin_user       = var.admin_user
  admin_password   = var.admin_password
  namespace_prefix = var.namespace_prefix
  admin_hash       = var.admin_hash
  env              = var.env
  domain           = var.domain
  s3_endpoint      = var.s3_endpoint
  s3_key           = var.s3_key
  s3_secret        = var.s3_secret
  branch_name      = var.dag_branch_name
  dag_path         = var.dag_path_name

}

module "jupyterhub" {
  source           = "./modules/jupyterhub"
  kube_context     = var.kube_context
  namespace_prefix = var.namespace_prefix
  domain           = var.domain
}
