variable "env" {
  type    = string
  default = "local"
}

variable "kube_config" {
  type    = string
  default = "~/.kube/config"
}

variable "kube_context" {
  type    = string
  default = "default"
}

variable "namespace_prefix" {
  type    = string
  default = "dnet-"
}

variable "domain" {
  type    = string
  default = "local-dataplatform"
}

variable "admin_user" {
  type    = string
}

variable "admin_password" {
  type    = string
}

variable "admin_hash" {
  type    = string
}

variable "s3_endpoint" {
  default = "https://minio.dnet-minio-tenant.svc.cluster.local"
}
variable "s3_key" {
  default = "minio"
}
variable "s3_secret" {
  default = "minio123"
}

variable "minio_buckets" {
  type    = list(map(string))
  default = [
    { name = "workflow-logs" },
    { name = "binaries" },
    { name = "graph" },
  ]
}

variable "dag_branch_name" {
  default = "master"
}

variable "dag_path_name" {
  default = "workflow/dnet"
}

