import os
from datetime import timedelta

from airflow.decorators import dag
from airflow.models.baseoperator import chain
from airflow.models.param import Param
from airflow.providers.cncf.kubernetes.operators.spark_kubernetes import SparkKubernetesOperator

from spark_configurator import SparkConfigurator

EXECUTION_TIMEOUT = int(os.getenv("EXECUTION_TIMEOUT", 6))

default_args = {
    "execution_timeout": timedelta(days=EXECUTION_TIMEOUT),
    "retries": int(os.getenv("DEFAULT_TASK_RETRIES", 1)),
    "retry_delay": timedelta(seconds=int(os.getenv("DEFAULT_RETRY_DELAY_SECONDS", 60)))
}


@dag(
    dag_id="orcid_enrichment_graph",
    dag_display_name="Enrich Graph with ORCID data",
    default_args=default_args,
    params={
        "S3_CONN_ID": Param("s3_conn", type='string', description="Airflow connection of S3 endpoint"),
        "ORCID_PATH": Param("s3a://graph/data/orcid_2023/tables", type='string', description=""),
        "INPUT_PATH": Param("s3a://graph/tmp/prod_provision/graph/07_graph_consistent", type='string', description=""),
        "OUTPUT_PATH": Param("s3a://graph/tmp/prod_provision/graph/09_graph_orcid_enriched", type='string',
                             description=""),
        "WRKDIR_PATH": Param("s3a://graph/tmp/prod_provision/working_dir/orcid_enrichment", type='string',
                             description="")
    },
    tags=["openaire"]
)
def orcid_enrichment_dag():
    chain(SparkKubernetesOperator(
        task_id='EnrichGraphWithOrcidAuthors',
        task_display_name='Enrich Authors with ORCID',
        namespace='dnet-spark-jobs',
        template_spec=SparkConfigurator(
            name="orcidenrich-{{ ds }}-{{ task_instance.try_number }}",
            mainClass="eu.dnetlib.dhp.enrich.orcid.SparkEnrichGraphWithOrcidAuthors",
            jarLocation='s3a://binaries/dhp-shade-package-1.2.5-SNAPSHOT.jar',
            arguments=["--orcidPath", "{{ dag_run.conf.get('ORCID_PATH') }}",
                       "--graphPath", "{{ dag_run.conf.get('INPUT_PATH') }}",
                       "--targetPath", "{{ dag_run.conf.get('OUTPUT_PATH') }}",
                       "--workingDir", "{{ dag_run.conf.get('WRKDIR_PATH') }}",
                       "--master", ""
                       ]).get_configuration(),
        kubernetes_conn_id="kubernetes_default"
    ),
        SparkKubernetesOperator(
            task_id='copyorcidenrichrels',
            task_display_name="Copy relations to ORCID Enriched graph",
            namespace='dnet-spark-jobs',
            template_spec=SparkConfigurator(
                name="copygroupedrels-{{ ds }}-{{ task_instance.try_number }}",
                mainClass="eu.dnetlib.dhp.oa.merge.CopyEntitiesSparkJob",
                jarLocation='s3a://binaries/dhp-shade-package-1.2.5-SNAPSHOT.jar',
                arguments=["--graphInputPath", "{{ dag_run.conf.get('INPUT_PATH') }}/",
                           "--outputPath", "{{ dag_run.conf.get('OUTPUT_PATH') }}/",
                           "--entities", "relation",
                           "--format", "text"
                           ]).get_configuration(),
            kubernetes_conn_id="kubernetes_default"
        )
    )


orcid_enrichment_dag()
