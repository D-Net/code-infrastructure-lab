import os
from datetime import timedelta

from airflow.decorators import dag
from airflow.models.baseoperator import chain
from airflow.models.param import Param
from airflow.providers.cncf.kubernetes.operators.spark_kubernetes import SparkKubernetesOperator

from spark_configurator import SparkConfigurator

EXECUTION_TIMEOUT = int(os.getenv("EXECUTION_TIMEOUT", 6))

default_args = {
    "execution_timeout": timedelta(days=EXECUTION_TIMEOUT),
    "retries": int(os.getenv("DEFAULT_TASK_RETRIES", 1)),
    "retry_delay": timedelta(seconds=int(os.getenv("DEFAULT_RETRY_DELAY_SECONDS", 60)))
}


@dag(
    dag_id="consistency_graph",
    dag_display_name="Enforce Consistency of Graph",
    default_args=default_args,
    params={
        "S3_CONN_ID": Param("s3_conn", type='string', description="Airflow connection of S3 endpoint"),

        "INPUT_PATH": Param("s3a://graph/tmp/prod_provision/graph/06_graph_dedup", type='string', description=""),
        "OUTPUT_PATH": Param("s3a://graph/tmp/prod_provision/graph/07_graph_consistent", type='string', description=""),
        "WRKDIR_PATH": Param("s3a://graph/tmp/prod_provision/working_dir/dedup", type='string', description=""),
        "IS_LOOKUP_URL": Param("http://services.openaire.eu:8280/is/services/isLookUp?wsdl", type='string',
                               description="")
    },
    tags=["openaire"]
)
def consistency_graph_dag():
    propagate_rel = SparkKubernetesOperator(
        task_id='PropagateRelation',
        task_display_name="Propagate Relations",
        namespace='dnet-spark-jobs',
        template_spec=SparkConfigurator(
            name="propagaterels-{{ ds }}-{{ task_instance.try_number }}",
            mainClass="eu.dnetlib.dhp.oa.dedup.SparkPropagateRelation",
            jarLocation='s3a://binaries/dhp-shade-package-1.2.5-SNAPSHOT.jar',
            arguments=["--graphBasePath", "{{ dag_run.conf.get('INPUT_PATH') }}",
                       "--graphOutputPath", "{{ dag_run.conf.get('OUTPUT_PATH') }}",
                       "--workingPath", "{{ dag_run.conf.get('WRKDIR_PATH') }}"
                       ]).get_configuration(),
        kubernetes_conn_id="kubernetes_default"
    )

    group_entities = SparkKubernetesOperator(
        task_id='GroupEntities',
        task_display_name="Group results by id",
        namespace='dnet-spark-jobs',
        template_spec=SparkConfigurator(
            name="groupentities-{{ ds }}-{{ task_instance.try_number }}",
            mainClass="eu.dnetlib.dhp.oa.merge.GroupEntitiesSparkJob",
            jarLocation='s3a://binaries/dhp-shade-package-1.2.5-SNAPSHOT.jar',
            arguments=["--graphInputPath", "{{ dag_run.conf.get('INPUT_PATH') }}",
                       "--checkpointPath", "{{ dag_run.conf.get('WRKDIR_PATH') }}/grouped_entities",
                       "--outputPath", "{{ dag_run.conf.get('OUTPUT_PATH') }}",
                       "--isLookupUrl", "{{ dag_run.conf.get('IS_LOOKUP_URL') }}",
                       "--filterInvisible", "true"
                       ]).get_configuration(),
        kubernetes_conn_id="kubernetes_default"
    )

    chain(propagate_rel, group_entities)


consistency_graph_dag()
