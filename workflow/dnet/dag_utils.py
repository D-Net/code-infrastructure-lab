from airflow.hooks.base import BaseHook
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

BUILD_PHASES = {
    "raw": "01_graph_raw",
    "grouped": "02_graph_grouped",
    "clean": "03_graph_cleaned",
    "resolve": "04_graph_resolved",

    "inference": "05_graph_inferred",
    "dedup": "06_graph_dedup",
    "consistency": "07_graph_consistent",
    "enrichment": "08_graph_dedup_enriched", # actionset
    "orcid_enhancement": "09_graph_orcid_enriched"
}

def get_bucket_name(context: dict, hook: S3Hook, param_name: str):
    bucket_name = context["params"][param_name]
    if not bucket_name:
        bucket_name = hook.extra_args['bucket_name']
    return bucket_name


def get_default_bucket():
    hook = S3Hook("s3_conn", transfer_config_args={'use_threads': False})
    try:
        return hook.service_config['bucket_name']
    except KeyError:
        return ''


GRAPH_ENTITIES = ["publication", "dataset", "otherresearchproduct", "software", "datasource", "organization", "project", "relation"]


GRAPH_ENTITIES_CLASS_NAMES = {
    "publication": "eu.dnetlib.dhp.schema.oaf.Publication",
    "dataset": "eu.dnetlib.dhp.schema.oaf.Dataset",
    "otherresearchproduct": "eu.dnetlib.dhp.schema.oaf.OtherResearchProduct",
    "software": "eu.dnetlib.dhp.schema.oaf.Software",
    "datasource": "eu.dnetlib.dhp.schema.oaf.Datasource",
    "organization": "eu.dnetlib.dhp.schema.oaf.Organization",
    "project": "eu.dnetlib.dhp.schema.oaf.Project",
    "relation": "eu.dnetlib.dhp.schema.oaf.Relation"
}